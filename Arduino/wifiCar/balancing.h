#ifndef BALANSING_H
#define BALANSING_H

#if PLATFORM_TYPE == BALANCING
void balancingbegin( );
void balansing_worker( );
void myPID( );
void PWMControl( );
#else
#define balancingbegin( ) 
#define balansing_worker( )
#endif

#endif
