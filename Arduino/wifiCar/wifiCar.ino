
#include "wificar.h"
extern "C" {
  #include "user_interface.h"
}

DCMotors motors( MEASURE_TIMECONST );

void setup()
{
  Serial.begin( CONSOLE_BAUDRATE );
  motors.begin( );
  begin_servo( );
  SPIFFS.begin();
  begin_http_server( );
  begin_ws( );
  begin_ota();
  //wifi_set_sleep_type( LIGHT_SLEEP_T );
}

  void loop( )
{
  http_server_worker( );
  ws_worker( );
  ota_worker( );
}
