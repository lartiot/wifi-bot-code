#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#if DEBUG_HTTP == 1
#define dbg_printf_http(...) do{ Serial.print( "[HTTP] " ); Serial.printf( __VA_ARGS__ ); Serial.println(); }while( 0 )
#else
#define dbg_printf_http(...)
#endif
#if DEBUG_WIFI == 1
#define dbg_printf_wifi(...) do{ Serial.print( "[WIFI] " ); Serial.printf( __VA_ARGS__ ); Serial.println(); }while( 0 )
#else
#define dbg_printf_wifi(...)
#endif
#if DEBUG_MDNS == 1
#define dbg_printf_mdns(...) do{ Serial.print( "[MDNS] " ); Serial.printf( __VA_ARGS__ ); Serial.println(); }while( 0 )
#else
#define dbg_printf_mdns(...)
#endif

/** -----------------------------------------------------
 * Init HTTP-server for wifiCar
 */
void begin_http_server( );
/** -----------------------------------------------------
 * Worker for HTTP-server 
 */
void http_server_worker( );

#endif
