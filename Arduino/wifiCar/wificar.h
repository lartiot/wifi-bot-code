#ifndef WIFICAR_H
#define WIFICAR_H

#include <Arduino.h>
#include <Servo.h>
#include <Ticker.h>
#include <FS.h>
#include "Wire.h"
#include "http_server.h"
#include "wificarws.h"
#include "wificarota.h"
#include "config.h"
#include "balancing.h"

#if DEBUG_SYS == 1
#define dbg_printf_sys(...) do{ Serial.printf( __VA_ARGS__ ); Serial.println(); }while( 0 )
#else
#define dbg_printf_sys(...)
#endif


/** ------------------------------------------------------------------------------------
 *  аппаратная инициализация выводов для сервомоторов
 */
void begin_servo( );

/** ------------------------------------------------------------------------------------
 *  Крайние варианты скорости
 */
enum
{
	STOP = 0,
    MAXSPEED = PWMRANGE,
    MAXPWR   = PWMRANGE,
};
/** ------------------------------------------------------------------------------------
 * Класс мотора 
 */
class DCMotor
{
public:
	DCMotor( int pwr, int dir ):PWM( pwr ), DIR( dir ){}
	
	void begin( )
	{  
		pinMode( PWM , OUTPUT );
		pinMode( DIR , OUTPUT );
	}
	void set( int power )
	{
		bool sign = power < 0;
		power = abs( power );		
		analogWrite( PWM , power );
		digitalWrite( DIR , sign ? LOW : HIGH );
	}
  void stop( ){ set( 0 ); }
private:
    /// Номера выводов для управления моторами
    const int PWM, DIR;
};

class DCMotors
{
public:
	DCMotors( int tc ): timeConstMs( tc ){}
	void begin( ) 
	{ 
		left.begin( ); right.begin( ); 
		left.stop( ); right.stop( );  
		//ticker.attach_ms( timeConstMs , ontick );
	}
	static void set( int lpwr , int rpwr );
	/// Объекты моторов
	static DCMotor left;	
	static DCMotor right;
protected:
	/// Таймер
	static Ticker ticker;
   static void ontick( );  
private:
      /// Постоянная времени
    const int timeConstMs;	
    static bool tic;
};
/*    ticker.attach_ms( timeConstMs , 
        [ ]
        ( DCMotor* obj )
        { Serial.println( obj->tic ); if( obj->tic ) obj->set( STOP ); } );
 */

#endif
