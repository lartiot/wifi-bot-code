#ifndef CONFIG_H
#define CONFIG_H

/** ============================================== 
                ТИПЫ ПЛАТФОРМ:
      0 - управление по приводу колес по левому и 
          правому борту или гусеничная платформа
      1 - управление колес рулевой машинкой
      2 - самобалансирующая платформа с опорой на 
          пару колес
    ============================================== */
#define TRUCK_TWO_WHEEL     0
#define TRUCK_WITH_SERVOS   1
#define BALANCING           2

/** ============================================== 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                НАСТРОЙКИ ПЛАТФОРМЫ
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ============================================== */
#define PLATFORM_TYPE     BALANCING     
//TRUCK_TWO_WHEEL
/** ============================================== */
/**  Управление сенсорами  ( 1-включен/0-выключен )*/
#define USING_ODOMETER        0
#define USING_SPEEDOMETER     0
#define USING_IMU             0
/** ============================================== */
/**  Управление моторами  ( 1-включен/0-выключен ) */
#define USING_MOTORS          1
#define USE_SERVO_1           1
#define USE_SERVO_2           1
#define USE_SERVO_3           0
#define USE_SERVO_4           0
/** ============================================== */
/// Диаметр колес в мм
#define WHEEL_DIAMETER_MM     ( 70.0f )
/// Количество вырезов на энкодере колеса
#define WHEEL_ENCODER_GAPS    6
/// Режим работы: 0 - без определения направления (1 сенсор на колесо)
///               1 - с определением  направления - квадратурный (2 сенсора на колесо)
#define WHEEL_ENCODER_QUADR   0
#define PIN_WHEEL_LEFT        100     
#define PIN_WHEEL_RIGHT       102
#if WHEEL_ENCODER_QUADR == 1
#define PIN_WHEEL_LEFT_Q      101     
#define PIN_WHEEL_RIGHT_Q     103
#endif
/// Постоянная времени измерений(семплирования) параметров колес
#define MEASURE_TIMECONST     20

/** ============================================== 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                НАСТРОЙКИ WIFI ПО УМОЛЧАНИЮ
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ============================================== */
// -----------------------------------------------------
/// Использовать в режиме клиента точки доступа (устройство подключается к AP)
#define USING_AP_CLIENT     1
// -----------------------------------------------------
/// Время ожидания соединения с AP в секундах 
#define AP_CLIENT_TO_S      20
// -----------------------------------------------------
/// TODO: Впишите название вашей точки и пароль к ней
///       Если вы используете шифрование WEP установите 1
///       в определении AP_CRYPT_WEP
#define AP_NAME             "SHL"           
#define AP_PASSWORD         "SCORPION256"
// -----------------------------------------------------
/// Использование встроенной AP
#define USING_SOFTAP        1
/// Имя и пароль для доступа к встроенной AP
#define SOFTAP_NAME         "ESP-CAR-AP-"
#define SOFTAP_PASSWORD     "admin"
// -----------------------------------------------------
/// Использовать загрузку ПО через Over-the-air programming (OTA)
#define USING_OTA_SERVER	0
// -----------------------------------------------------
/// Использовать mDNS сервер
#define USING_MDNS_SERVER   1
/// Имя mDNS сервера
#define MDNS_SERVER_NAME    "esp-car"
/** ============================================== 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        НАСТРОЙКИ ВЫВОДА ОТЛАДОЧНОЙ ИНФОРМАЦИИ
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ============================================== */
// -----------------------------------------------------
/// Вывод отладочной информации Web-socket'a
#define DEBUG_WS              1
/// Вывод отладочной информации HTTP-сервера
#define DEBUG_HTTP            1
/// Вывод отладочной информации ota-сервера
#define DEBUG_OTA             0
/// Вывод системных сообщений
#define DEBUG_SYS             1
/// Вывод системных сообщений
#define DEBUG_SENS            1
/// Вывод сообщений WIFI подсистемы
#define DEBUG_WIFI            1
/// Вывод сообщений подсистемы инерциальной навигации
#define DEBUG_IMU             1


/// ===============================================================================
///   Не меняйте следующие параметры 
/// ===============================================================================
#define WEBSOCKET_TCPPORT   8081
#define HTTPSERVER_TCPPORT  80
#define CONSOLE_BAUDRATE    115200

/* -----------------------------------------------------
    IO Pins numbers
*/
#define PIN_MOTORA_POWER      14
#define PIN_MOTORB_POWER      12
#define PIN_MOTORA_DIRECTION  0
#define PIN_MOTORB_DIRECTION  2
#define PIN_SERVO_1           15
#define PIN_SERVO_2           16
#define PIN_SERVO_3           1
#define PIN_SERVO_4           2

/** -----------------------------------------------------
 *   Derivatives constant
 */
#ifndef M_PI
#define M_PI                  ( 3.1415f )
#endif

#if ( USING_ODOMETER == 1 || USING_SPEEDOMETER == 1 ) && defined( WHEEL_DIAMETER_MM ) && defined( WHEEL_ENCODER_GAPS )

#define WHEEL_LENGTH           	( WHEEL_DIAMETER_MM * 3.1415f )
#define WHELL_ENC_SERCTOR		( WHEEL_LENGTH  / WHEEL_ENCODER_GAPS )

#endif


#endif
