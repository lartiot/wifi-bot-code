#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <ArduinoOTA.h>
#include "config.h"
#include "wificarota.h"

void begin_ota( )
{
#if USING_OTA_SERVER == 1
//  // Start OTA server.
//  	String hostname( SOFTAP_NAME );
//	hostname += String( ESP.getChipId(), HEX );
//	dbg_printf_ota( "Start OTA server." );
//	ArduinoOTA.setHostname( (const char *)hostname.c_str() );
//	ArduinoOTA.begin( );	
//	dbg_printf_ota( "OTA server started." );
#endif

//    MDNS.begin(host);

//    httpUpdater.setup(&httpServer);
//    httpServer.begin();

//    MDNS.addService("http", "tcp", 80);
}

void ota_worker()
{
#if USING_OTA_SERVER == 1 
	ArduinoOTA.handle( );
	yield( );
#endif	
}
/**/
