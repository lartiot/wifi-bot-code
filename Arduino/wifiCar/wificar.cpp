#include "wificar.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <Servo.h>

//void MotorsDC :: tickerCB( ) {
  //Serial.println( "ticker-callback" );
//}
//Ticker MotorsDC :: ticker;

Ticker DCMotors :: ticker;

bool DCMotors :: tic = false;

void DCMotors :: set( int lpwr , int rpwr )
{
   left.set( lpwr ); 
   right.set( rpwr );
}

void DCMotors :: ontick( ) 
{
 // Serial.println( "ticker-callback" );
}

DCMotor DCMotors :: left ( PIN_MOTORA_POWER , PIN_MOTORA_DIRECTION );
DCMotor DCMotors :: right( PIN_MOTORB_POWER , PIN_MOTORB_DIRECTION );
