#ifndef WIFICAROTA_H
#define WIFICAROTA_H

#if DEBUG_OTA == 1
#define dbg_printf_ota(...) do{ Serial.print("[OTA ] "); Serial.printf( __VA_ARGS__ ); Serial.println(); }while( 0 )
#else
#define dbg_printf_ota(...)
#endif
/** ----------------------------------------------
 * Start web-socket interface
 */
void begin_ota( );
/** ----------------------------------------------
 * ws-worker for loop() function
 */
void ota_worker( );


#endif
